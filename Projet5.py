#LINFO1102 ; Projet 5 : Lightnovel
#Louis, Thomas, Jorge et Chris
#30/04/19, groupe P5.2.4

from flask import Flask
from flask_ask import Ask, statement, question

app = Flask(__name__)
ask = Ask(app, '/')


@ask.launch
def start():
    return question('Bonjour, quel age as-tu ?')


@ask.intent('histoireIntent')
def lage(age):                                     #Demande l'age pour donner une histoire qui interesse l'user
    try:
        if int(age) < 1 or int(age) > 77:
            return statement("Vous n'avez pas entré un age correct.")
        return histoire(int(age))
    except:
        return statement("Vous n'avez pas entré un age correct ou il y a eu une erreur.")

def histoire(age):
    index = 0           #Variable très utile pour savoir où nous sommes dans l'histoire
    global index
    if 1 <= age <= 12:  #histoire pour enfant
        return hi1(0)
    elif age <= 18:     #histoire pour adolescent
        return hi2(0)
    elif age <= 77:     #histoire pour adulte
        return hi3(0)

def hi1(ind):
    global index        #global pour pouvoir l'indenter et garder le fils sur toutes les fonctions.
    if ind == 0:
        return question("Salut, je m'appelle Beps, Beps l'archéologue, tu connais ma soeur Dora l'exploratrice et son ami Babouche." +
                        " Je pars pour une aventure incroyable, tu veux m'accompagner ?" + "a) Oui avec plaisir b) Non je ne suis pas convaincu")
    if ind == 1:
        index += 1
        return question("Trop cool, on va tellement bien s'amuser ensemble. Alors voila le topo: j'ai été informé qu'apparemment des T-Rex auraient vécus près de la cote africaine."
                        " En tant qu'archéologue je vais vérifier que des ossements n'y soient pas enterrés." + " On peut y aller soit en bateau, soit en avion, qu'est-ce-que tu préfères ?"
                        + " a) Bateau b) Avion")
    if ind == 2:
        index -= 2
        return question("Allez s'il te plait, j'ai vraiment besoin de ton aide." + " Tu veux bien m'accompagner ? a) d'accord b) Non je ne veux pas")

    if ind == 3:
        index += 1
        return question("Parfait, on pourra observer les dauphins et les baleines pendant le voyage." + " (trois heures plus tard)" + " AHH, nous voila enfin arrivés."
                        + " Bon il est temps qu'on aille regarder de plus près ses ossements de T-Rex" + " Mais d'ailleurs sais-tu ce qu'est un T-Rex?" +
                        " a) Oui b) Non")

    if ind == 4:
        return question("Oui je crois que c'est mieux, on voudrait pas se prendre un iceberg en chemin." + " (deux heures plus tard)" + " AHH, nous voila enfin arrivés."
                        + " Bon il est temps qu'on aillee regarder de plus près ses ossements de T-Rex" + " Mais d'ailleurs sais-tu ce qu'est un T-Rex?"
                        + "a) Oui b) Non")

    if ind == 5:
        index += 1
        return question("Et bien dis donc, je ne savais pas que j'avais un connaisseur à mes cotés." + " (quatre heures de marche plus tard)" + " Ouf, nous voila enfin arrivés sur les lieux."
                        + " Bon maintenant il faudrait juste savoir où fouiller." + " Tu penses qu'il vaut mieux aller fouiller à la plaine ou en haut de la montagne ?"
                        + " a) montagne b) plaine")

    if ind == 6:
        return question("Et bien le T-Rex était en quelques sortes le roi des dinosaures." + " C'était un carnivore et un prédateur redoutable qui vivait dans des terrains plats et"
                        " qui ne laissait aucune chance à ses proies. Trouver des ossements de T-Rex aujourd'hui est quelque chose de très rare," + "mais si on en trouve, la satisfaction sera énorme."
                        + " Mais quelque chose me dit qu'avec toi a mes cotés on trouvera ses ossements c'est sur." + " (quatre heures de marche plus tard)"
                        + " Ouf, nous voila enfin arrivés sur les lieux." + " Bon maintenant il faudrait juste savoir où fouiller." +
                        " Tu penses qu'il vaut mieux aller fouiller à la plaine ou en haut de la montagne ?" + "a) montagne b) plaine")

    if ind == 7:
        return question("Il parait qu'ils vivaient dans des endroits plats mais allons voir là-haut. (une heure d'ascension plus tard) Commençons à fouiller (après une heure de fouille)"
                        + " Bon je ne pense pas qu'on trouvera quelque chose ici, tu veux bien qu'on aille voir à la plaine ? a) Oui allons-y b) Non restons, je suis sur qu'on trouvera")

    if ind == 8:
        return statement("(une fois arrivé à la plaine) Allez au boulot, nous avons des ossements de T-Rex à trouver ! (une heure de fouille plus tard) OH! Voila les ossements qu'on"
                         + " cherchait ! Merci mille fois, je n'aurais jamais réussi à les trouver sans ton aide !")

    if ind == 9: statement("(après une heure de rechercher en plus) Eh mais quelque chose dépasse du sol là-bas ! Mais oui c'est un ossement, il n'a pas l'air d'appartenir à un T-Rex"
                           +" mais nous avons trouvé un ossement de dinausaure, merci mille fois pour ton aide !")

def hi2(ind):
    return statement("Il n'y a pas encore d'histoire pour adolescent disponible.")

def hi3(ind):
    return statement("Il n'y a pas encore d'histoire pour adulte disponible.")

@ask.intent('choisi')
def choix(choix):        #intent qui récupère si l'user réponds a ou b dans les choix qu'il fait
    global index
    if choix is None:
        return statement("Veuillez choisir a ou b, s'il vous plait")
    if choix == 'a':
        index += 1
        return hi1(index)
    if choix == 'b':
        index += 2
        return hi1(index)



if __name__ == '__main__':
    app.run()

