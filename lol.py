from flask import Flask
from flask_ask import Ask, statement, question

app = Flask(__name__)
ask = Ask(app, '/')


@ask.launch
def start():
    return question('Bonjour, quel age as-tu ?')


@ask.intent('testintent')
def lage(age):
    try:
        if int(age) < 1 or int(age) > 69:
            return statement("Vous n'avez pas entré un age correct.")
        return histoire(int(age))
    except:
        return statement("Vous n'avez pas entré.")

def histoire(age):
    if 1 <= age <= 10:
        return hi1()
    elif age <= 15:
        return hi2()
    elif age <= 25:
        return hi3()
    elif age <= 69:
        return hi4()

def hi1():
    return statement("Salutation")

def hi2():
    return statement("Bonjour")

def hi3():
    return statement("Bonsoir")

def hi4():
    return statement("Tard plus")


if __name__ == '__main__':
    app.run()

